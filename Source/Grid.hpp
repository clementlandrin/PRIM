// Grid.h
#ifndef GRID_HPP // include guard
#define GRID_HPP

#include "Cell.hpp"

#include "../External/glm/glm/glm.hpp"
#include <vector>

class Particle;

class Grid
{
public:
  Grid(int resolution, float width, float height, float depth);

  inline const glm::vec3 GetPosition() { return m_Position; };
  inline const float GetCellWidth() { return m_CellWidth; };
  inline const float GetCellHeight() { return m_CellHeight; };
  inline const float GetCellDepth() { return m_CellDepth; };
  inline const std::vector<std::vector<std::vector<Cell>>> GetCells() { return m_Cells; };

  inline void SetPosition(glm::vec3 position) { m_Position = position; };
  inline void SetCellWidth(float cellWidth) { m_CellWidth = cellWidth; };
  inline void SetCellHeight(float cellHeight) { m_CellHeight = cellHeight; };
  inline void SetCellDepth(float cellDepth) { m_CellDepth = cellDepth; };

  glm::vec3 ComputeCellIndicesFromPosition(glm::vec3 position);
  void InitParticleCellIndices(std::vector<Particle*> particles);

private:
  glm::vec3 m_Position;
  float m_CellWidth;
  float m_CellHeight;
  float m_CellDepth;
  std::vector<std::vector<std::vector<Cell>>> m_Cells;
};

#endif /* GRID_HPP */
