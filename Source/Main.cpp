#include "../External/glm/glm/glm.hpp"

#include <iostream>

#include "Fluid.hpp"
#include "Grid.hpp"

#define VISCOSITY 0.0f
#define DENSITY 0.0f
#define INITIAL_ENERGY 0.0f
#define PARTICLE_NUMBER 100
#define RESOLUTION 100
#define SCENE_WIDTH 100.0f
#define SCENE_HEIGHT 100.0f
#define SCENE_DEPTH 100.0f

int main (int argc, char ** argv)
{
  bool update_positions = true;

  Fluid* fluid = new Fluid(VISCOSITY, DENSITY, INITIAL_ENERGY);
  fluid->GenerateParticlesUniformly(PARTICLE_NUMBER, 
                                    glm::vec3(SCENE_WIDTH/2.f, 
                                              SCENE_HEIGHT/3.f, 
                                              SCENE_DEPTH/2.f), 
                                              SCENE_WIDTH/4.f, 
                                              SCENE_HEIGHT/4.f, 
                                              SCENE_DEPTH/4.f);
  Grid* grid = new Grid(RESOLUTION, SCENE_WIDTH, SCENE_HEIGHT, SCENE_DEPTH);
  grid->InitParticleCellIndices(fluid->GetParticles());

  while(update_positions)
  {
    fluid->UpdateParticlePositions(grid);
  }

  delete fluid;
  std::cout << "Exiting Main" << std::endl;
	return 0;
}
