// Cell.hpp
#ifndef CELL_HPP // include guard
#define CELL_HPP

#include "Particle.hpp"

#include "../External/glm/glm/glm.hpp"
#include <vector>

class Cell
{
public:
	inline void AddParticle(Particle* particle) { m_Particles.push_back(particle); };
	inline void EraseParticleAtIndex(int index) { if(index < m_Particles.size()) { m_Particles.erase(m_Particles.begin()+index);} };
  inline std::vector<Particle*> GetParticles() { return m_Particles; };

private:
	glm::vec3 m_Speed;
  glm::vec3 m_Luminance;
	std::vector<Particle*> m_Particles;
};

#endif /* CELL_HPP */