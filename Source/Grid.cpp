#include "Grid.hpp"

#include <iostream>

#include "Particle.hpp"

Grid::Grid(int resolution, float width, float height, float depth)
{
  m_Cells.resize(resolution);
  for (int i = 0; i < resolution; i++)
  {
    m_Cells[i].resize(resolution);
    for (int j = 0; j < resolution; j++)
    {
      m_Cells[i][j].resize(resolution);
    }
  }

  m_CellWidth = width / resolution;
  m_CellHeight = height / resolution;
  m_CellDepth = depth / resolution;
}

glm::vec3 Grid::ComputeCellIndicesFromPosition(glm::vec3 position)
{
  int i = position[0] / m_CellWidth;
  int j = position[1] / m_CellHeight;
  int k = position[2] / m_CellDepth;
  
  return glm::vec3(i,j,k);
}

void Grid::InitParticleCellIndices(std::vector<Particle*> particles)
{
  for (int i = 0; i < particles.size() ; i++)
  {
    glm::vec3 indices = ComputeCellIndicesFromPosition(particles[i]->GetPosition());
    Cell cell = m_Cells[indices[0]][indices[1]][indices[2]];
    particles[i]->SetIndexInCell(cell.GetParticles().size());
    cell.AddParticle(particles[i]);
    std::cout << "Pushed particle in Cell [" << indices[0] << ", " << indices[1] << ", " << indices[2] << "]" << std::endl;
  }
}