#include "Fluid.hpp"

#include <iostream>
#include <cstdlib>
#include <ctime>

Fluid::~Fluid()
{
  for (Particle* particle : m_Particles)
  {
    delete particle;
  }
  m_Particles.clear();
}

void Fluid::GenerateParticlesUniformly(int particleNumber, glm::vec3 origin, float width, float height, float depth)
{
  srand (static_cast <unsigned> (time(0)));
  glm::vec3 position;
  glm::vec3 initialSpeed;
  for (int i = 0; i < particleNumber; i++)
  {
    float x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/width));
    float y = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/height));
    float z = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/depth));
    position = glm::vec3( x, y, z);
    initialSpeed = glm::normalize(position);

    Particle* particle = new Particle(position, m_InitialEnergy/particleNumber);
    particle->SetSpeed(initialSpeed);

    m_Particles.push_back(particle);
    std::cout << "Generated Particle at position (" << x << ", " << y << ", " << z << ")" << std::endl;
  }

}

void Fluid::UpdateParticlePositions(Grid* grid)
{
  for (int i = 0; i < m_Particles.size(); i++)
  {
    m_Particles[i]->UpdatePosition(m_Particles[i]->GetPosition() + glm::vec3(0.1f)*m_Particles[i]->GetSpeed(), grid);
  }
}
