#include "Particle.hpp"

#include "Grid.hpp"

#include <iostream>

void Particle::UpdatePosition(glm::vec3 new_position, Grid* grid)
{
    glm::vec3 new_cell_indices = grid->ComputeCellIndicesFromPosition(new_position);
    glm::vec3 current_cell_indices = grid->ComputeCellIndicesFromPosition(m_Position);
    if (new_cell_indices != current_cell_indices)
    {
        std::cout << "Particle moved from Cell [" << current_cell_indices[0] << ", " << current_cell_indices[1] << ", " << current_cell_indices[2] 
            << "] to Cell [" << new_cell_indices[0] << ", " << new_cell_indices[1] << ", " << new_cell_indices[2] << "]" << std::endl;
        Cell cell = grid->GetCells()[new_cell_indices.x][new_cell_indices.y][new_cell_indices.z];
        cell.EraseParticleAtIndex(m_IndexInCell);
        cell.AddParticle(this);
    }
    m_Position = new_position;
}