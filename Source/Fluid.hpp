// Fluid.hpp
#ifndef FLUID_HPP // include guard
#define FLUID_HPP

#include <vector>
#include "../External/glm/glm/glm.hpp"

#include "Particle.hpp"

class Fluid
{
public:
  Fluid(float viscosity, float density, float initialEnergy) { m_Viscosity = viscosity;
                                                               m_Density = density;
                                                               m_InitialEnergy = initialEnergy; };
  ~Fluid();

  void GenerateParticlesUniformly(int particleNumber, glm::vec3 origin, float width, float height, float depth);
  void UpdateParticlePositions(Grid* grid);
  
  inline const float GetViscosity() { return m_Viscosity; };
  inline const float GetDensity() { return m_Density; };
  inline const float GetInitialEnergy() { return m_InitialEnergy; };
  inline const std::vector<Particle*> GetParticles() { return m_Particles; };

  inline void SetViscosity(float viscosity) { m_Viscosity = viscosity; };
  inline void SetDensity(float density) { m_Density = density; };
  inline void SetInitialEnergy(float initialEnergy) { m_InitialEnergy = initialEnergy; };

private:
  float m_Viscosity;
  float m_Density;
  float m_InitialEnergy;
  std::vector<Particle*> m_Particles;
};

#endif /* Fluid_HPP */
