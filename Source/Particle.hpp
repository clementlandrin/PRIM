// Particle.hpp
#ifndef PARTICLE_HPP // include guard
#define PARTICLE_HPP

#include "../External/glm/glm/glm.hpp"

class Grid;

class Particle
{
public:
  Particle(glm::vec3 position, float energy) { m_Position = position; m_Energy = energy;};

  inline const glm::vec3 GetPosition() { return m_Position; };
  inline const glm::vec3 GetSpeed() { return m_Speed; };
  inline const float GetEnergy() { return m_Energy; };
  inline const int GetIndexInCell() { return m_IndexInCell; };

  void UpdatePosition(glm::vec3 position, Grid* grid);
  inline void SetSpeed(glm::vec3 speed) { m_Speed = speed; };
  inline void SetEnergy(float energy) { m_Energy = energy; };
  inline void SetIndexInCell(int index) { m_IndexInCell = index; };

private:
  glm::vec3 m_Position;
  glm::vec3 m_Speed;
  float m_Energy;
  int m_IndexInCell;
};

#endif /* PARTICLE_HPP */
